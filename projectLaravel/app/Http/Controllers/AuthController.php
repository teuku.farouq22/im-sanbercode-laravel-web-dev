<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('halaman.register');
    }

    public function kirim(Request $request)
    {
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];

        return view('halaman.welcome', ['namaDepan'=> $namaDepan, 'namaBelakang'=> $namaBelakang]);
    }
}
