<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }

    public function store(Request $request)
    {
        // validasi data
        $request->validate([
            'nama' => 'required|min:5',
            'umur' => 'required|min:2',
            'bio' => 'required|min:5',
        ]);

        // masukkan data request ke table cast di database
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]);

        // Kita lempar ke halaman /cast
        return redirect('/cast');

    }

    public function index()
    {
        $cast = DB::table('cast')->get();
        // dd($cast);
        return view('cast.tampil', ['cast' => $cast]);
    }

    public function show($id)
    {
        $cast = DB::table('cast')->find($id);

        return view('cast.detail', ['cast' => $cast]);
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->find($id);

        return view('cast.edit', ['cast' => $cast]);
    }

    public function update($id, Request $request)
    {
        // validasi data
        $request->validate([
            'nama' => 'required|min:5',
            'umur' => 'required|min:2',
            'bio' => 'required|min:5',
        ]);

        // update data
        DB::table('cast')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']
                ]
            );
        
        // lempar ke url /cast
        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
