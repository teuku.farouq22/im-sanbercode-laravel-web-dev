
@section('content')
@extends('layout.master')

@section('judul')
Halaman Tampil Edit Cast
@endsection

@section('content')
<form method="POST" action="/cast/{{$cast->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Cast Nama</label>
      <input type="text" value="{{$cast->nama}}" name='nama' class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Cast Umur</label>
        <input type="number" value="{{$cast->umur}}" name='umur' class="form-control">
      </div>
      @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Cast Bio</label>
      <textarea name="bio" class='form-control' cols="30" rows="10">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection


@endsection