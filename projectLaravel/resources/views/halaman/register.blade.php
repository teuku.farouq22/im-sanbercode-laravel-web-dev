@extends('layout.master')
@section('judul')
Sign Up Form
@endsection

@section('content')
    <form action="/welcome" method="POST">
        @csrf
        <label for="">Nama Depan</label> <br>
        <input type="text" name="fname"> <br> <br>
        <label for="">Nama Belakang</label> <br>
        <input type="text" name="lname"> <br> <br>
        <label for="">Gender</label> <br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other <br> <br>
        <label for="">Nationality: </label> 
        <select name="nationality" id="">
            <option value="">Indonesian</option>
            <option value="">American</option>
            <option value="">African</option>
            <option value="">Arabian</option>
        </select> <br><br>
        <label for="">Language Spoken</label> <br> 
        <input type="checkbox" name="language"> Bahasa Indonesia <br>
        <input type="checkbox" name="language"> English <br>
        <input type="checkbox" name="language"> Other <br> <br>
        <label for="">Bio</label> <br> 
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br> <br>
        <input type="submit" value="Sign Up">

    </form>
@endsection
