<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'dashboard']);
Route::get('/register', [AuthController::class, 'daftar']);
Route::post('/welcome', [AuthController::class, 'kirim']);

Route::get('/data-tables', function(){
    return view('halaman.datatable');
});

Route::get('/table', function(){
    return view('halaman.table');
});

// CRUD

// Create Data
// route untuk mengarah  ke form tambah cast
Route::get('/cast/create', [CastController::class, 'create']);
// route untuk menyimpan data ke Db table cast
Route::post('/cast', [CastController::class, 'store']);

// Read Data
// route untuk menampilkan semua data yang ada di table cast databse
Route::get('/cast', [CastController::class, 'index']);
// route untuk ambil detail berdasarkan id
Route::get('/cast/{id}', [CastController::class, 'show']);

// Update Data
// route untuk mengarah ke form edit cast dengan membawa data berdasarkan id
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
// route untuk update cast berdasarkan id
Route::put('/cast/{id}', [CastController::class, 'update']);

// Delete Data
// route hapus data berdasarkan id cast
Route::delete('/cast/{id}', [CastController::class, 'destroy']);

